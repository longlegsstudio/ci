#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;
using System.IO;

class BitriseUnity
{
	public static void Build()
	{		
		BitriseTools tools = new BitriseTools ();
		tools.PrintInputs ();

		BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
		buildPlayerOptions.scenes = tools.GetActiveScenes();//NOTIFY USER IF NO ANY
		buildPlayerOptions.locationPathName = tools.inputs.buildOutput;

		if (tools.inputs.buildPlatform == BitriseTools.BuildPlatform.android) {
			EditorPrefs.SetString ("AndroidSdkRoot", tools.inputs.androidSdkPath);